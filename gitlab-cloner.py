#!/usr/bin/env python
"""Recursively clones all repositories given a starting group."""

import argparse
import os
import pathlib

import git
from gitlab import Gitlab

def main():
    """Perform the heart of the script."""
    args = parse_cli_arguments()

    gitlab = Gitlab(url=args.url, private_token=args.token)

    projects = []
    if args.all:
        projects = gitlab.projects.list(include_subgroups=True, iterator=True)
    else:
        groups = gitlab.groups.list(search=args.group, order_by="path", include_subgroups=False)
        if len(groups) == 1:
            print(f"Processing group {groups[0].full_path}..")
            projects = groups[0].projects.list(include_subgroups=True, as_list=False)
        else:
            matches = []
            for group in groups:
                if group.full_path == args.group:
                    matches.append(group)
                elif group.name == args.group:
                    matches.append(group)

            if len(matches) == 1:
                print(f"Processing group {matches[0].full_path}..")
                projects = matches[0].projects.list(include_subgroups=True, as_list=False)
            else:
                print("Multiple group matches found:")
                for group in matches:
                    print(f"  {group.full_path}")

    for project in projects:
        project = gitlab.projects.get(project.id)
        print(f"Processing project: {project.path_with_namespace} ({project.id})")

        repo_path = f"{args.root_dir}/{project.path_with_namespace}"
        if os.path.isdir(repo_path):
            # exists
            print("  Already cloned.")
            if args.user_email:
                configure_user(repo_path, args.user_email)
        else:
            clone_project(repo_path, project.ssh_url_to_repo)
            if args.user_email:
                configure_user(repo_path, args.user_email)


def clone_project(repo_path, repo_url):
    """Clone the repository locally.

    Args:
        repo_path: Path to the local repository.
        repo_url: The GitLab URL of the remote repository.
    """
    print(f"  Cloning to {repo_path}")
    git.Repo.clone_from(repo_url, repo_path)


class TimeoutException(Exception):  # Custom exception class
    """Custom exception class which we use for our timeout decorator."""

    pass


def configure_user(repo_path, email):
    """Configure the user's email in the locally cloned repository.

    Args:
        repo_path: Path to the local repository.
        email: Email address of the user.
    """
    repo = git.Repo(repo_path)
    with repo.config_writer() as git_config:
        print(f"  Configuring user email: {email}")
        git_config.set_value("user", "email", email)


def parse_cli_arguments():
    """Parse CLI arguments.

    Returns:
        An argparse object.
    """
    parser = argparse.ArgumentParser(
        description="""
          Clone all subordinate repositories for the given group, repeating the
        Gitlab hierarchy in the defined local root directory.
        """,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "--url",
        type=str,
        help="Gitlab server URL",
    )
    parser.add_argument(
        "--token",
        type=str,
        default=os.getenv("GITLAB_API_TOKEN"),
        help="Gitlab user token providing API access",
    )
    group = parser.add_mutually_exclusive_group()
    group.add_argument(
        "--group",
        type=str,
        help="Name of the Gitlab group to clone from",
    )
    group.add_argument(
        "--all",
        action="store_true",
        help="Clone all repos",
    )
    parser.add_argument(
        "--root-dir",
        type=str,
        default=f"{pathlib.Path.home()}/devel",
        help="Root directory to clone hierarchy to",
    )
    parser.add_argument(
        "--user-email",
        type=str,
        help="Your email address used to configure git",
    )

    args = parser.parse_args()
    return args


if __name__ == "__main__":
    main()
